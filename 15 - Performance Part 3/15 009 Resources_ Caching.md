

To learn more about how caching works and the two headers we discussed in the
previous videos: Etag and Cache Control, I recommend you read further using
these resources:  
  
1\. [Caching Everywhere](https://medium.freecodecamp.org/the-hidden-
components-of-web-caching-970854fe2c49)

2\. [Cache
Headers](https://developers.google.com/web/fundamentals/performance/optimizing-
content-efficiency/http-caching)  
  
3\. [Caching and
Performance](https://devcenter.heroku.com/articles/increasing-application-
performance-with-http-cache-headers)

