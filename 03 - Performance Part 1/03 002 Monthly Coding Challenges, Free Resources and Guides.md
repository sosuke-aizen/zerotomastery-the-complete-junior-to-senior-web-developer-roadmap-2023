

Every month in the community, we will have [**coding
challenges**](https://zerotomastery.io/community/coding-challenges/) for you
to participate in, [**monthly industry
blogs**](https://zerotomastery.io/blog/) and other ****[**top free
resources**](https://zerotomastery.io/resources/) emailed to you. We also have
annual events like _Advent of Code_ , _Hacktoberfest_ , and _Frosty February
Hackathon_, plus some _giveaways_ as well.  
  
Make sure you have your email settings on Udemy to allow this, as every month
we will be emailing you a community update email where we list all of the new
challenges, free resources, videos and giveaways.  
  
Some students have mentioned they do not receive these emails and it's mainly
because of this _(Go to Your profile Icon and click:_ ** _Account >
Notification Settings_** _)_. Make sure you have these 2 boxes checked if you
want to receive the monthly email like in the image below.

![](https://img-c.udemycdn.com/redactor/raw/article_lecture/2021-03-09_14-13-22-603210afd08757facbdaf09b687c3c54.png)

  
  
  
 **PS. Udemy will be asking to leave a review throughout the course. Please
leave a review as it really helps out the course and allows more people to
discover us in this massive marketplace :)**  
  
\- Andrei

