

Want to learn more about Image file types? I recommend you check out
theseresources:  
  
<https://99designs.com/blog/tips/image-file-types/>  
  
<https://pageweight.imgix.com/>  
  
<https://www.sitepoint.com/gif-png-jpg-which-one-to-use/>  
  

