

One thing that I didn't mention much in the previous video is that because of
HTMLparsing, the location of the script tags when you put `async` or`defer`
attributes on them is important. This is a great resource that explains the
importance of this:<https://stackoverflow.com/questions/10808109/script-tag-
async-defer>

