

There is another technique that can be beneficial when optimizing websites. It
is easy to misuse, and different browsers have different performance metrics
for them so I did not include it in the videos. However, if you are interested
and would like to read more about it, this is (In my opinion) the best
resource on the topic:  

<https://css-tricks.com/prefetching-preloading-prebrowsing/>

