

Attached, you will find the files I used for the previous videos. Try
practicing the skills I mentioned to really solidify your knowledge. You can
experiment on different browsers and see how they behave differently, or even
expand the project on your own. Enjoy!

Share your changes and improvements in the **  #juniortosenior** Discord
channel to show off once you are done :)  

