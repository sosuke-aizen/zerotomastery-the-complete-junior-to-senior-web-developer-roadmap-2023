

Using all you have learned in this section, it's time to impress Bruno and
optimize the Keiko Corp Website which isn't very good right now.You can find
the project [repository here](https://github.com/aneagoie/keiko-corp). Fork
the repository, and create a [Github
pages](https://help.github.com/articles/configuring-a-publishing-source-for-
github-pages/) for it so that you have a URL you can test on these two tools:  
  
1.[PageSpeed
Insights](https://developers.google.com/speed/pagespeed/insights/)  
2.[WebPageTest](https://www.webpagetest.org/)  
  
One you have done all you can, post your results and time in our Discord
Community! Let's see who can make the fastest website!  
  
P.s. Don't worry about the code for now. We are purely focused on performance
improvements we have covered in this section.

