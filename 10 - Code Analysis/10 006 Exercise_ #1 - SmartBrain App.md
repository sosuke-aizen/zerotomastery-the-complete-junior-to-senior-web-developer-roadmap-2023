

To get started with your local setup of the SmartBrain app, follow the
previous video and the below resources:  
  
1. Client side code: <https://github.com/aneagoie/smart-brain>  
2\. Server side code: <https://github.com/aneagoie/smart-brain-api>  
  
The above projects were part of  **The Complete Web Developer: Zero to
Mastery. If you would like to see how the app was built, I walk you through
the entire step in that  course. **However, since this is a course on going
from Junior to Senior Developer, we are assuming you are familiar with a
simple front end and backend setup. We are skipping the basic building of an
app like this since it is most likely something that you have encountered
before and you should be familiar with. But don't worry, we take you through
it nice and slow through the remaining sections.  
  
  
 **A few things you will need:  
#1**  You will need to add your own [Clarifai API
key](https://www.clarifai.com/) in `controllers/image.js`  (which is free) on
the server:  
  
 **#2** A postgreSQL database if you haven't done so already in the Security
section of the course:  
  
To install PostgreSQL on your computer, follow the below steps OR you can
watch the next optional video:

**Mac:** Follow my previous video for instructions. You will need
[homebrew](https://brew.sh/) for the easies way to set up. keep in mind you
may have to run with the 'sudo' command.  Or you can follow [this
tutorial](https://www.codementor.io/engineerapart/getting-started-with-
postgresql-on-mac-osx-are8jcopb).

[PSequel GUI](http://www.psequel.com/)

**Windows:** Follow [this tutorial](http://www.postgresqltutorial.com/install-
postgresql/)

**Linux: Thanks to fellow student Dimitris for this great guide:**

For any of the Linux users following the course and interested in installing
PostgreSQL along with a GUI (eg. pgAdmin), [their website has wonderful
instructions](https://www.postgresql.org/download/), and so does their wiki
(for example, [this link is for Debian and Ubuntu based
distros](https://wiki.postgresql.org/wiki/Apt)).  

Also, one way to issue the commands you typed in the video to start, stop,
restart PostgreSQL in Linux is:

    
    
    sudo systemctl start postgresql     # starts the server
    sudo systemctl stop postgresql      # stops it
    sudo systemctl restart postgresql   # restart it
    sudo systemctl status postgresql    # check the server's status
    

The "`createdb test` " command and the "`psql 'test'` " command are the same
(at least for Debian/Ubuntu systems) from what I saw.

When it's first installed, PostgreSQL just has the 'postgres' user, and the
way to initially enter PostgreSQL is by typing  `sudo su - postgres` , and
then `psql` .  After Andrei creates the 'test' database, we can create a user
with the same name as our current logged in user, to be a database
administrator. This way we can just type in `psql 'test'`  from the command
line and enter the database without the need of logging in as the 'postgres'
user, just like Andrei does in the lecture. This can be done with `CREATE USER
your-user-name-here WITH SUPERUSER;` , and we can verify that he was created
with `\du` . Now we can exit by typing `\q`  and then `exit` , and enter our
database just like Andrei does, with `psql 'test'` .

Lastly, with pgAdmin4 we need to create a connection with the server the first
time we use it, and this is done by right-clicking 'Servers' on the left pane,
and choosing 'Create' > 'Server'. We give our server a name, and in the
'Connection' tab we type in 'localhost' as the host, just like Andrei shows in
the lecture, and press 'Save'.

