

Heads up! In the next video we will be setting up our environment to check out
some code! I will be using a MAC to demonstrate this, but for those who are
using a Windows machine, we have provided some extra videos in the section
**Extras: For Windows Users** to help you along and demonstrate some of the
tricky parts on a Windows machine :)

