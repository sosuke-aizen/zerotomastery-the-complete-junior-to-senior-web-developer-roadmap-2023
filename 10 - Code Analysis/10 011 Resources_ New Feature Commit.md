

You can see the new code change from the previous video and the new feature
added [in this commit history](https://github.com/aneagoie/smart-brain-
boost/commit/09961431e44692473533cfdd8526aaa8bce5a0be).

How did you implement the new feature? Share on the **#js** channel on Discord
and compare your work with others!

