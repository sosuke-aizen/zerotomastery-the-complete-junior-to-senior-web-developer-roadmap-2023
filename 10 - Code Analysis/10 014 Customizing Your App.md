

As we move through the course, I recommend you apply what you learn and
challenge yourself as you customize your app. For example, this student:  
  
" **So while watching the videos for the Smart Brain face detection final
project, I decided to create the project using NextJS. Additionally, after
doing the parts with bcrypt and password hashing I opted to instead give a
shot at implementing 3rd party authorizations using Auth0. Lastly I went with
my own layout and CSS so I could hone those skills. It was a lot of fun and
work to figure out how to properly do things in NextJS alongside of Andrei
utilizing create-react-app. I learned a lot about react hooks and other react
concepts as well as next js and the solutions they bring to the table. So
thank you @Andrei Neagoie for the wonderful course. Onto the bonus stuff at
the end!"  
  
**[ **https://github.com/waffleflopper/smart-brain-
nextjs**](https://github.com/waffleflopper/smart-brain-nextjs
"https://github.com/waffleflopper/smart-brain-nextjs") ****

  

