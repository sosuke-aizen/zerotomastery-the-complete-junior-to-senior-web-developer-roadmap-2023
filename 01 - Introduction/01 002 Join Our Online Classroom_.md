

To make sure that you all succeed with this course, we have an online
community of hundreds of thousands of students _(I know, it's a big
community!)_ helping to answer questions and talk about all things
programming, technology, and business! You can also [speak to past students
who are now successful students](https://zerotomastery.io/testimonials)
working at some of the top tech companies and get advice from them.  
[  
](https://discord.gg/nVmdHYY)[ **CLICK HERE TO JOIN
NOW**](https://discord.gg/9KxUUxt7Vd)

 **  
** _(Heads up! In some cases, you may get an "invalid invite" when you click
above. This is due to your browser plugins and you can fix it by opening the
link above in a different browser or in incognito mode)_  

A lot of companies use Discord to communicate across teams therefore it is a
valuable tool for you to learn as you become a developer. In the spirit of
keeping the course as useful as possible for employment this year, we are
introducing a Discord online classroom where you can:

 **-  Introduce yourself  
\- Ask the community questions  
\- Get help with exercises  
\- Meet other students around the world  
\- Learn how to answer questions  
\- Learn from each other**

Many students have said this is the best part of the course as you will feel
like you are learning in a classroom rather than by yourself in front of a
computer. I know it isn't for everybody, but being able to ask questions and
help others when they have questions is the best way to learn. **The Discord
community should give you faster replies to your questions from the community,
while learning another valuable tool of a programmer. **  

**Once there,  you can sign up and click on different channels to explore
different topics.  If you are new to Discord we have written up a quick
tutorial for you right
**[**here**](https://zerotomastery.io/community/discord) **.**  
  
I'll see you inside!  

\- Andrei  

[**CLICK HERE TO JOIN NOW**](https://discord.gg/9KxUUxt7Vd)

  

![](https://img-c.udemycdn.com/redactor/raw/article_lecture/2021-03-09_13-54-24-8b2ccf9752b026233a7ebd195d792254.png)

  

