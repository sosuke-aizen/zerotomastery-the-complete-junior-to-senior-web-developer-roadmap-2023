

 **Before we get started, let's do a quick exercise that won't take you more
than 5 minutes.  **  
  
 **#1  **Please introduce yourself in the community Discord group in the
**#introductions** channel to get familiar with how it works! Quickly share
who you are, where you are from, and why you chose to do this course. Doing
this course yourself is completely fine, but it is always good to have a
community of others, similar to you, helping you when you need motivation, and
sharing in the joy when you succeed.  
  
 **#2  **Go to the **#programming-buddies** and try to find a buddy who is
starting the course today just like you!  **You will be keeping each other
accountable throughout the course and motivating each other to finish.  **I
know it may sound silly, but this is the biggest factor in increasing your
chances of completing the course. Who knows, maybe you will find life long
friends/coworkers this way.  
  
 **There is also a special channel for all of the students of this course:
#juniortosenior**

  

 **#3** _(Optional)_ **One of our students has made a video walkthrough for
you of the community**[ **here**](https://youtu.be/TVI_MBoCOak) **if you want
to watch and learn more about how everything works.**  

Some of the channels available for you to join are:  

 ** _#javascript_** _\- For all JavaScript questions  
_ ** _#dev-resources_** _\- Myself and others share interesting articles and
tools we find. Great way to stay up to date in the industry  
_ ** _#alumni_** _\- Ask graduates of this course questions  
_ ** _#womenintech_** _\- For the female coders out there  
_ ** _#job-hunting_** _\- Anything related to finding a job as a developer  
_ ** _#code100_** _\- To join a community of students dedicated to code every
day for 100 days. You can join anytime.  
_

 _....and many many more!_

 **Trust me, the Discord community is an invaluable part of this course (some
would say, it is  the best part of this course). You can also follow me on
twitter where I keep you updated about industry news, upcoming courses, and
send announcement about coding challenges we run monthly:
**[**@andreineagoie**](https://twitter.com/AndreiNeagoie)

  

 **PS. Udemy will be asking to leave a review throughout the course. Please
leave a review as it really helps out the course and allows more people to
discover us in this massive marketplace :)**

