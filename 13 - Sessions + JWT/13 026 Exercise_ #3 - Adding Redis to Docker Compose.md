

As you saw in the previous video, we need to add the Redis service to our
Docker Compose file. See if you can get this set up on your own by using what
you have learned in previous videos. If you get stuck, you can either go ask
for help in the **#juniortosenior**  channel on Discord, or you can watch the
next video for the solution.  
  
Have fun!

