

Over the rest of the section, we are going to be building a pretty big feature
of our SmartBrain App. The best thing you can do in this case is to code along
with me and have your own version of the SmartBrain App on your computer. You
should have your backend Dockerized by now, but if you have yet to do so, I
recommend you check out **Code Analysis** section and the **Docker** section
of this course. You can also find the [github repository
here](https://github.com/aneagoie/smart-brain-boost-api-dockerized) of the
finalized docker backend, however, it is recommended that you have your own
setup that works on your machine using [this repository of the original smart-
brain-api.](https://github.com/aneagoie/smart-brain-api)  
  
Finally, you can grab along the front end files [from
here](https://github.com/aneagoie/smart-brain). The first few videos will be
covering the front end portion of the SmartBrain App.  
  
At the end of the section, I will be providing you with the
repositoriescontaining the final project as well.  
  
Have fun!

