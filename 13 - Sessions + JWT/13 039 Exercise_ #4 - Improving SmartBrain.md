

Wow, we've really come a long way with the Smart Brain App. However, I bet you
noticed many areas that we can improve the application. You see, software is
always a constantly evolving journey. As you gain more experience you start to
notice areas you can improve the code, and areas you can refactor. This is
what you want. It means you are connecting the dots and understanding the
information flow between different systems. To continue to practice this
skill, I have a few tasks for you. Just like in the real world, there won't be
a solution file for you at the end of this one. It either works or it doesn't.
Using all you have learned up to this point, using all of your resources
(including our community on Discord), see if you can implement these features:  
  
  

 **Task 1** : Add `pet`  and `age`  column to the database and allow users to
update these fields on their profile.

 **Task 2** : Implement token flow as we have done so far for `/register`  end
point as well.

 **Task 3** : Add Sign out functionality where you revoke the token when a
user signs out of the app  
  
 **Task 4 (Bonus)** : You may notice a lot of repeated code (especially when
it comes to fetch()). How can you improve this? Try to refactor the code and
perhaps also improve the logic to make the app even better!  
  
You can find the github repositories of the code we have worked on up to this
point below:  
[Front End](https://github.com/aneagoie/smart-brain-boost/)  
[Back End](https://github.com/aneagoie/smart-brain-boost-api)  
[Back End - Dockerized](https://github.com/aneagoie/smart-brain-boost-api-
dockerized)  
  
Enjoy the exercise... it's a tough one!

