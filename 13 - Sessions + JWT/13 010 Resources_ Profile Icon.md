

Here are the two libraries we saw in the previous video:  
  
1\. [Tachyons](https://tachyons.io/components/)  
  
2\. [Reactstrap](https://reactstrap.github.io/)  
  
Using component libraries in React is one of the great things about using
component architecture like in React. We are able to build things really fast
and use components already built by the community so that we can worry about
the core logic of our applications (which is always more fun!).

