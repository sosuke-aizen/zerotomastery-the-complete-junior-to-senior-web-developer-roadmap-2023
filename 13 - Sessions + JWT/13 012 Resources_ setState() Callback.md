

There are many times you may want to use a callback function when using
setState(). You can read up on the topic a little more in detail here:  
  
1.<https://medium.com/@voonminghann/when-to-use-callback-function-of-setstate-
in-react-37fff67e5a6c>  
  
2.<https://stackoverflow.com/questions/42038590/when-to-use-react-setstate-
callback>

