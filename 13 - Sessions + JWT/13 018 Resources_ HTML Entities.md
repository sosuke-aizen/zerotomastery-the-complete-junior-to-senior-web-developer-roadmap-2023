

To take a quick look at some HTML entities that you can
use:<https://www.w3schools.com/charsets/ref_html_entities_4.asp>  
  
By the way, the reason we aren't spending a lot of time on CSS in this course
is because we are focused more on the big picture items. I promise we are
getting to a really good part of the course, so bare with me while we get some
basic front end design set up so we can implement some exciting sessions and
authenticated route logic in a few videos!

