

Truly, thank you for completing the course. I hope you are able to do what you
want with this knowledge now. I would love to hear from you about your
experiences and where this course has gotten you in your life.  **  You can
also follow me on twitter where I will announce when my next course is out (I
am currently working on it!): **[
**@andreineagoie**](https://twitter.com/AndreiNeagoie) It will really mean a
lot to me if you are able to leave a positive review if you have enjoyed this
course. Reviews mean everything on a website like Udemy, and it is the only
way I can be discovered by others.  Please keep in touch and share this course
to anybody that may benefit from it. Until next time. Buh bye.  

  P.S. I have created the **  #alumni** channel on Discord. If you have
finished the course I highly recommend you join the channel and stay up to
date and network throughout your careers. Thanks for @Alex Castro for this
idea. It would be interesting to have the alumni follow up on their career
journeys. Like * **Did they find a new job**?* or * **Did they enroll  in some
further study?*** or even * **Did they launch  their own business/product?***
Many students would benefit from this and I hope you give back a bit to the
community :)

