00:00:00 - 01 001 Course Overview
00:11:17 - 01 004 First Day Of Work



00:00:00 - 02 001 Brunos Request
00:00:38 - 02 002 Introduction to SSH



00:00:00 - 02 003 SSH Command
00:07:12 - 02 006 Saving The Day Through SSH
00:13:46 - 02 009 How SSH Works
00:15:15 - 02 010 Symmetric Encryption
00:19:36 - 02 011 Asymmetric Encryption
00:26:23 - 02 013 Hashing
00:33:17 - 02 014 Passwords Or SSH_
00:36:44 - 02 015 SSH Into A Server
00:49:58 - 02 018 Exercise_ Set Up SSH on Github
00:53:09 - 02 020 Solution_ Set Up SSH on Github
00:58:07 - 02 022 Section Summary



00:00:00 - 03 001 Brunos Request
00:01:18 - 03 003 Introduction to Performance Part 1
00:04:59 - 03 004 3 Keys To Performance
00:09:33 - 03 005 Network Performance
00:14:18 - 03 006 Image File Formats
00:19:04 - 03 008 Image Optimizations
00:26:08 - 03 009 Image Optimizations 2
00:38:33 - 03 011 Delivery Optimizations
00:43:20 - 03 013 Exercise_ #2 - Network Optimizations
00:48:09 - 03 014 Solution_ #2 - Network Optimizations
00:53:20 - 03 015 Critical Render Path Introduction
00:58:30 - 03 016 Critical Render Path 1
01:04:31 - 03 017 Critical Render Path 2
01:21:58 - 03 018 Critical Render Path 3
01:35:41 - 03 020 Critical Render Path 4
01:39:39 - 03 022 Exercise_ #4 - Keiko Corp Website
01:43:39 - 03 024 Solution_ Keiko Corp Website
01:53:02 - 03 027 HTTP_2
01:55:45 - 03 030 Section Summary



00:00:00 - 04 001 IMPORTANT_ Thinking Like A Senior Developer
00:02:12 - 04 003 Brunos Request
00:03:39 - 04 004 Section Overview
00:06:00 - 04 006 Angular vs React vs Vue
00:12:42 - 04 009 CWD_ Introduction To React.js
00:25:04 - 04 010 CWD_ Create React App
00:32:12 - 04 011 CWD_ React App Folder Structure
00:40:27 - 04 012 CWD_ React Fundamentals
00:44:22 - 04 013 CWD_ React Fundamentals 2
00:49:22 - 04 014 CWD_ Class vs Functional App.js
00:53:24 - 04 015 CWD_ Hooks vs Classes



00:00:00 - 04 017 CWD_ Your First React Component
00:20:13 - 04 018 CWD_ Building A React App 1
00:36:16 - 04 021 CWD_ Building A React App 2
00:46:03 - 04 022 CWD_ Building A React App 3
01:12:38 - 04 023 CWD_ Styling Your React App
01:17:38 - 04 025 CWD_ Building A React App 4
01:33:08 - 04 026 CWD_ Building A React App 5
01:42:01 - 04 027 CWD_ Building A React App 6
01:51:07 - 04 029 CWD_ Keeping Your Projects Up To Date
01:59:37 - 04 031 Solution_ React 17
02:03:34 - 04 032 CWD_ React Review
02:06:09 - 04 033 CWD_ Error Boundary In React
02:14:20 - 04 034 CWD_ Deploying Our React App
02:19:30 - 04 035 CWD_ React Hooks
02:23:13 - 04 036 CWD_ React Hooks 2
02:27:00 - 04 037 CWD_ React Hooks 3
02:30:26 - 04 038 CWD_ React Hooks 4
02:34:27 - 04 039 CWD_ React Hooks 5
02:38:12 - 04 040 CWD_ React Hooks 6
02:45:07 - 04 041 CWD_ React Hooks 7
02:54:23 - 04 042 CWD_ React Hooks 8
02:59:12 - 04 044 CWD_ React Hooks 9
03:05:11 - 04 045 Introduction To Redux And Webpack
03:08:00 - 04 046 State Management
03:15:13 - 04 047 Why Redux_
03:26:52 - 04 048 Installing Redux
03:32:37 - 04 049 Redux Toolkit
03:34:40 - 04 050 Redux Actions And Reducers
03:48:24 - 04 051 Redux Store And Provider
03:56:59 - 04 052 Redux connect()
04:13:31 - 04 053 Redux Middleware
04:22:16 - 04 054 Redux Async Actions
04:51:43 - 04 055 Redux Project Structures
04:56:38 - 04 057 Popular Tools For React + Redux
05:04:19 - 04 058 Module Bundlers
05:11:08 - 04 059 Introduction To Webpack
05:13:53 - 04 061 Webpack
05:51:54 - 04 062 Updating Libraries_ Babel 7 + ESlint
05:58:29 - 04 065 Parcel
06:07:49 - 04 067 Section Summary



00:00:00 - 05 001 Brunos Request
00:01:06 - 05 002 Section Overview
00:03:09 - 05 004 Optimizing Code
00:15:24 - 05 005 Code Splitting Introduction
00:21:19 - 05 007 Code Splitting Part 1
00:35:46 - 05 009 Code Splitting Part 2
00:48:19 - 05 010 Code Splitting Part 3
01:01:48 - 05 011 Code Splitting Part 4



00:00:00 - 05 013 Solution_ React.lazy() Code Splitting



00:00:00 - 05 014 React Performance Optimizations
00:10:36 - 05 016 React Performance Optimizations 2
00:30:02 - 05 018 Optimizing Code Review
00:33:32 - 05 020 Progressive Web Apps
00:46:26 - 05 023 Progressive Web Apps Examples
00:52:41 - 05 024 PWA - HTTPS
00:58:53 - 05 026 PWA - App Manifest
01:05:06 - 05 028 PWA - Service Workers
01:18:22 - 05 031 PWA - Final Thoughts
01:20:25 - 05 034 Deploying Our React App
01:30:44 - 05 035 Service Worker Updates



00:00:00 - 05 036 Solution Part 1 - PWA
00:08:52 - 05 037 Solution Part 2 - PWA
00:19:01 - 05 039 Section Summary



00:00:00 - 06 001 Brunos Request
00:01:10 - 06 002 Section Overview
00:07:32 - 06 004 Types of Tests
00:10:31 - 06 005 Testing Libraries
00:25:59 - 06 007 Unit Tests
00:28:41 - 06 008 Integration Tests
00:31:28 - 06 009 Automation Testing
00:35:56 - 06 010 Final Note On Testing



00:00:00 - 06 011 Setting Up Jest



00:00:00 - 06 012 Our First Tests
00:11:10 - 06 013 Writing Tests
00:16:41 - 06 015 Asynchronous Tests
00:27:00 - 06 016 Asynchronous Tests 2



00:00:00 - 06 018 Mocks and Spies



00:00:00 - 06 022 Introduction To Enzyme
00:14:29 - 06 025 Snapshot Testing
00:23:59 - 06 026 Snapshot Testing + Code Coverage
00:28:53 - 06 028 Testing Stateful Components
00:40:09 - 06 029 Quick Recap
00:42:44 - 06 031 Testing Connected Components
01:06:34 - 06 032 Testing Connected Components 2
01:10:10 - 06 034 Testing Reducers
01:25:07 - 06 035 Testing Actions
01:43:34 - 06 038 Section Summary



00:00:00 - 07 001 Brunos Request
00:01:00 - 07 002 Section Overview
00:02:28 - 07 004 Dynamic vs Static Typing
00:14:18 - 07 005 Weakly vs Strongly Typed
00:17:37 - 07 006 Static Typing In Javascript
00:27:23 - 07 007 TypeScript Compiler



00:00:00 - 07 010 TypeScript
00:11:33 - 07 012 TypeScript 2
00:14:49 - 07 013 TypeScript 3
00:22:14 - 07 015 TypeScript 4
00:27:53 - 07 016 TypeScript 5
00:34:32 - 07 017 TypeScript 6
00:39:21 - 07 019 TypeScript 7
00:42:55 - 07 021 TypeScript 8
00:49:00 - 07 022 TypeScript 9
00:50:22 - 07 023 TypeScript 10
00:51:45 - 07 024 DefinitelyTyped
00:57:42 - 07 025 Update_ Create React App with TypeScript
01:01:45 - 07 026 Exercise_ #1 - TypeScript In Robofriends
01:08:56 - 07 028 Solution_ #1 - TypeScript In Robofriends
01:22:56 - 07 030 A Little Joke
01:23:46 - 07 031 Section Summary



00:00:00 - 08 001 Brunos Request
00:00:58 - 08 002 Section Overview
00:08:52 - 08 003 CSR vs SSR
00:14:26 - 08 004 Server Side Rendering React
00:25:22 - 08 006 CSR vs SSR Part 2
00:36:40 - 08 007 SSR React Libraries
00:40:38 - 08 008 Static vs SSR vs CSR_ Gatsby.js vs Next.js vs React.js
00:47:47 - 08 010 Setting Up Next.js
00:55:43 - 08 011 Next.js Pages
01:03:18 - 08 012 Client Side Routing
01:05:44 - 08 014 Shared Components
01:07:58 - 08 015 Dynamic Apps with Next.js
01:23:29 - 08 017 Deploying Next.js Apps



00:00:00 - 08 018 Updating To Latest Version Of Next.js
00:03:21 - 08 021 Section Summary



00:00:00 - 09 001 Brunos Request
00:01:14 - 09 002 Section Overview
00:05:21 - 09 003 Star Of Security
00:06:24 - 09 004 Injections
00:26:06 - 09 008 3rd Party Libraries
00:36:57 - 09 010 Logging
00:51:00 - 09 011 HTTPS Everywhere
00:57:21 - 09 012 XSS + CSRF



00:00:00 - 09 015 Code Secrets



00:00:00 - 09 016 Secure Headers
00:05:35 - 09 018 Access Control
00:12:07 - 09 019 Data Management
00:23:01 - 09 021 Don8217t Trust Anyone
00:25:55 - 09 022 Authentication
00:29:05 - 09 025 Section Summary



00:00:00 - 10 001 Brunos Request
00:01:30 - 10 002 Section Overview
00:05:26 - 10 005 Setting Up Your Environment
00:32:28 - 10 007 Optional_ CWD - Installing PostgreSQL
00:39:21 - 10 008 How To Analyze Code
00:55:59 - 10 012 Solution_ #3 - Multiple Face Detection
01:13:19 - 10 013 Section Summary



00:00:00 - 11 001 Brunos Request
00:01:09 - 11 002 Section Overview
00:05:56 - 11 004 Docker Containers
00:16:55 - 11 005 Installing Docker
00:19:19 - 11 007 Dockerfile
00:27:56 - 11 009 Docker Commands
00:33:05 - 11 010 Dockerfile 2
00:41:09 - 11 013 Docker Compose
00:54:25 - 11 015 Docker-Compose 2
00:58:22 - 11 017 Docker Compose 3
01:00:16 - 11 018 Docker Compose 4
01:14:03 - 11 020 Docker Compose 5
01:16:12 - 11 021 Docker Compose 6
01:29:01 - 11 023 Docker Compose 7
01:34:16 - 11 027 Section Summary



00:00:00 - 12 001 Brunos Request
00:01:27 - 12 002 Section Overview
00:07:58 - 12 004 Introduction To Databases
00:18:52 - 12 005 Installing Redis
00:23:40 - 12 007 Redis Commands
00:29:37 - 12 008 Redis Data Types
00:31:37 - 12 009 Redis Hashes
00:33:37 - 12 010 Redis Lists
00:37:33 - 12 011 Redis Sets + Sorted Sets
00:43:44 - 12 012 Section Summary



00:00:00 - 13 001 Section Overview
00:02:20 - 13 002 Cookies vs Tokens
00:13:38 - 13 004 What We Are Building
00:18:32 - 13 005 JWT
00:21:56 - 13 007 Project Goals
00:27:15 - 13 009 Profile Icon
00:41:09 - 13 011 Profile Dropdown
00:54:07 - 13 013 Profile Styling
01:05:11 - 13 014 Profile Modal 1
01:09:36 - 13 015 Profile Modal 2
01:32:19 - 13 016 Profile Modal 3
01:44:52 - 13 017 Profile Modal 4
01:52:01 - 13 019 Updating Profile 1
02:01:32 - 13 021 Updating Profile 2
02:11:41 - 13 022 Updating Profile 3
02:19:55 - 13 023 User Authentication
02:35:05 - 13 024 Sending The JWT Token
02:51:26 - 13 025 Adding Redis
02:58:14 - 13 027 Solution_ #3 - Adding Redis To Docker Compose
03:03:43 - 13 028 Storing JWT Tokens
03:12:00 - 13 029 Retrieving Auth Token
03:19:17 - 13 030 Client Session Management
03:33:34 - 13 032 Session Sign In
03:44:06 - 13 033 Authorization Middleware
03:58:42 - 13 034 Fixing A Bug
04:01:28 - 13 035 Reviewing Our Code
04:04:27 - 13 036 Updating Our App
04:08:37 - 13 037 Optional_ Why bcrypt-nodejs_
04:10:37 - 13 038 Section Summary



00:00:00 - 14 001 Brunos Request
00:01:30 - 14 002 Section Overview
00:06:56 - 14 004 Amazon Web Services
00:18:19 - 14 005 Monolithic vs Micro Services
00:21:17 - 14 006 Amazon Lambda
00:27:20 - 14 007 Amazon Lambda Dashboard
00:33:10 - 14 009 Serverless
00:38:40 - 14 010 Serverless 2
00:43:47 - 14 011 IAM
00:48:24 - 14 013 Deploying A Function
00:59:51 - 14 014 Deploying A Function 2
01:07:13 - 14 015 Deploying A Function 3
01:20:21 - 14 016 Quick Bug Fix
01:21:47 - 14 017 Section Summary



00:00:00 - 15 001 Brunos Request
00:01:10 - 15 002 Section Overview
00:02:07 - 15 003 CDNs
00:08:46 - 15 004 GZIP
00:14:17 - 15 005 Database Scaling
00:25:40 - 15 006 Caching 1
00:32:38 - 15 007 Caching 2
00:40:55 - 15 008 Caching 3
00:52:26 - 15 010 Load Balancing
00:58:37 - 15 011 Nginx 1
01:05:11 - 15 013 Nginx 2
01:20:18 - 15 015 Section Summary



00:00:00 - 16 001 Brunos Request
00:01:33 - 16 002 Section Overview
00:03:17 - 16 004 Continuous Integration, Delivery, Deployment
00:13:28 - 16 005 Building Great Software
00:20:40 - 16 007 CircleCI
00:37:28 - 16 008 Continuous Integration 1
00:47:40 - 16 009 Continuous Integration 2
00:56:53 - 16 011 Building Great Software 2
01:02:57 - 16 012 Section Summary



00:00:00 - 17 001 Section Overview
00:01:26 - 17 002 Complexity vs Simplicity
00:05:20 - 17 003 NPM Is Not Your Friend
00:07:32 - 17 004 Learn To Learn
00:09:31 - 17 005 Start With Why



00:00:00 - 18 003 The One You Have Been Waiting For



00:00:00 - 19 001 Introduction From Wolfgang
00:01:03 - 19 002 Git for Windows
00:07:09 - 19 003 Install NodeJS for Windows
00:09:53 - 19 004 Install PostgreSQL for Windows
00:16:36 - 19 005 Using PSQL and PGADMIN



00:00:00 - 20 001 AMA - 100,000 Students__.encrypted






