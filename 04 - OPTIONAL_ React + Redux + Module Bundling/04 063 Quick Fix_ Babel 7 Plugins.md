

Just a heads up on a quick correction from the previous video. To add a plugin
to babel compiler you would need to have it under the plugin property in
package.json file like this:

    
    
    {
      "babel": {
        "presets": [ ... ],
        "plugins": [ ... ],
      }
    }

