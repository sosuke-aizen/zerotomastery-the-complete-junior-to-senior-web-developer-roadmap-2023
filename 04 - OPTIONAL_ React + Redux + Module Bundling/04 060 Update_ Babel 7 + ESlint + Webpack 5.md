

Heads up! In the next video we are going to set up our own project using
Webpack and other tools that are popular in the industry. However, libraries
always change as they get new updates. Since the next video, Babel, Webpack
and ESlint have newer versions, so in the video following the next one, I will
show you how to update this configuration to work with the latest Babel and
Eslint. When you install Webpack you should automatically get the latest
version but all the code you will see in the next videos will work!  
  
 **One piece of advice: _Webpack configuration is notoriously complex and
confusing. As a matter of fact, Webpack team is constantly updating the
library with new ways of doing things. Therefore I don't recommend coding
along in the next video with me.  Simply watch and understand the core
principles I teach you. It is vary rare that you will have to configure
Webpack like I will show you in the next video, and it is better to avoid
getting too deep into Webpack. I will just show you some principles so that in
the future if you ever do need to create an entire webpack setup yourself, you
at least know where to start. _**

