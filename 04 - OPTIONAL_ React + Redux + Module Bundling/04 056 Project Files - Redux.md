

Here is the Github repo with the complete project files for the RoboFriends
app which we built together with Redux. Go ahead and clone it and do what you
will with it. It's all yours to put into your portfolio, or make it even
better!  
  
<https://github.com/aneagoie/robofriends-redux>

  

As a bonus, one of the ZTM students created this project using React Hooks
which you can check it out here: <https://github.com/rusty-jnr/robofriends>

