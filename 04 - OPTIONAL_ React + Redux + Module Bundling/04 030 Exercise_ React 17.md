

Now that you know how to update your project dependencies to the latest
versions, it time to try it yourself.

  

[ **React version 17 was recently
announced**](https://reactjs.org/blog/2020/08/10/react-v17-rc.html)! Luckily
for us there are no breaking changes, so go ahead and update the `react` and
`react-dom` packages to v17 in your `package.json` file and see if everything
still works!

