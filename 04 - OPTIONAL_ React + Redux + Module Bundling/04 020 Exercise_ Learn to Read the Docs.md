

As a developer, you need to develop the muscle of being able to read the
documentation website when you don't understand something. Here is a quick
exercise:  
  
What does `<React.StrictMode>` we see inside the `index.js` file do? Go look
through the React documentation website and see if you can find out. You can
find the answer at the link below!  
  
  
  
<https://reactjs.org/docs/strict-mode.html>

