

To learn more about the 3 techniques used in Enzyme for testing: shallow,
mount, render, have a look at [their api
documentation](http://airbnb.io/enzyme/docs/api/) to see what kind of methods
you can use to test your components. We will be using some of them in the
later parts ofthis section.

