

As always, you should always consider asking why and challenging common
assumptions and make the decisions that make sense for a specific project.
Here is an excellent article that argues an uncommon opinion when it comes to
testing,with validpoints:<https://blog.usejournal.com/lean-testing-or-why-
unit-tests-are-worse-than-you-think-b6500139a009>

