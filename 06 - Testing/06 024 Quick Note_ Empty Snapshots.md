

HEADS UP! Depending on your version of Jest, in the next video, you may find
that your snapshots aren't created properly (empty). This is a known issue
with Jest and may require you to do the following:
<https://backbencher.dev/blog/empty-shallowwrapper-snapshot-jest-enzyme>. I
have included the code change mentioned in the article in this [git
repo](https://github.com/aneagoie/robofriends-testing) to show you how to do
this if you encounter the issue.  
  
You'll learn all about Snapshot testing next!

