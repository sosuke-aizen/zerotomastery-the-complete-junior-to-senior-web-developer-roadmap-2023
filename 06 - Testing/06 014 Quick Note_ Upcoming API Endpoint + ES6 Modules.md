

Heads up! 2 things to be aware of in the next video:  
  
 **1\. ES6 Module import is here in Node! In the next video, we will be using
a library called**` **node-fetch**` **. We recommend you install version 2 of
this library instead of the still new version 3. This is because version 3 may
cause problems for you depending on what version of Node you have installed on
your computer (ES6 module errors).**[ **You can always use this github repo as
a reference (my code).**](https://github.com/aneagoie/testing-exercise)  
  
  
 **2\. I am going to introduce you to an API endpoint that sometime may or may
not be available (SWAPI API). To make sure you are still able to run the code
properly, I have added a few other options for you to use so that you make
sure the API endpoint we will be testing works. You can use any of the below
APIs that work in the next video:**

  

I recommend using:
[**http://swapi.py4e.com/api/people**](http://swapi.py4e.com/api/people/)

  

[ **Star Wars API**](http://swapi.py4e.com/) **or**[ **Star Wars API
(clone)**](https://swapi.dev/)

[ **Numbers API**](http://numbersapi.com/#42)

[ **Chuck Norris API**](https://api.chucknorris.io/)

[ **Pokemon API**](https://pokeapi.co/)

