

Using [this Github repo](https://github.com/aneagoie/robofriends-testing)
which contains the tests you saw in my videos for the robofriends app,
continue to practice and add your own tests and see if you can improve the
code coverage. Use this as a playground to try different packages, techniques,
and methods that you want so you can improve your skill and be comfortable
with testing. Share your coverage report in the **#juniortosenior** Discord
channel to show off once you are done :)

