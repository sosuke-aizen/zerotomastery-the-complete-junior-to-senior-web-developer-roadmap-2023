

Have a look [at this Github repo](https://github.com/aneagoie/testing-
exercise) that contains the tests we have written from the previous videos. Go
ahead and start practicing some tests by writing your own function and
learning to use Jest using the cheat sheet I provided for you. Use this as a
playground to improve your skills and get comfortable with using a testing
framework like Jest. Good luck!

