

In the next video we are going to discuss my favourite library to use with
React: _Enzyme_. An alternative to Enzyme is [React Testing
Library](https://testing-library.com/). They are both very similar in what
they do, so if you learn one, it is very easy to use the other as you can see
[here](https://testing-library.com/docs/react-testing-library/migrate-from-
enzyme/).  
  
If you are using Enzyme with React 17 you may encounter some issues since as
of now, they are working on the official update for [Enzyme React 17
adapter](https://github.com/enzymejs/enzyme/pull/2430). This should be added
to the library in a few days. For now you can use this alternative _(a simple
google search of "Enzyme adapter for React 17" would have revealed this to
you)_ : <https://stackoverflow.com/questions/64658031/which-enzyme-adapter-
works-with-react-17>  
  
You will learn more about how to use this in the next video!

