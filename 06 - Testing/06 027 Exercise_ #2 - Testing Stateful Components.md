

Here is a challenge for you! This is something we will cover in the next
video, but I want you to practice reading documentation and solving a problem
that you have partial information to (which happens a lot when testing). Can
you try and write tests for the CounterButton component and get the code
coverage to 100%? Give it a try and see you in the next video!

