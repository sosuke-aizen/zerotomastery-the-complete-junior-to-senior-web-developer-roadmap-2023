

Moving forward, you should use the [Jest Cheat
Sheet](https://github.com/sapegin/jest-cheat-sheet) to help you along as you
write tests if you are coding along in the videos. It will also come in handy
at the end of this course where you will have to write tests for our
robofriends app.

