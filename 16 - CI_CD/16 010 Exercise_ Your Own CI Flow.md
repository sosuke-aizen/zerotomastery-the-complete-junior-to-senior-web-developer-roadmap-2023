

Using what you have learned in the previous videos, pick any of your current
Github project and implement a CircleCI pipeline for each pull request! Good
luck, and remember: If you ever get stuck, we have a large community on
Discord who you can reach out to for help.You can also use[this
repository](https://github.com/aneagoie/robofriends-ci) which has the files
used from the videos you just watched.

