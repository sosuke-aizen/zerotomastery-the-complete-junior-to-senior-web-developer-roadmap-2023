

There are a lot of good options for tools out there. You can have a look at
this article for a run down of all of them:<https://code-
maze.com/top-8-continuous-integration-tools/>

As I stated in the video, most companies use one of these tools and my
personal recommendations are:  
  
TravisCI orCircleCI for hosted CI servers.  
  
Jenkins for your own managedCI servers.  

  

