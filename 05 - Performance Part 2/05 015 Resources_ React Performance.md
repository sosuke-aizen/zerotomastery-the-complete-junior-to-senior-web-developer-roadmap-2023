

Moving forward, I recommend you install this plugin into your chrome browser
to help you with React development:  
[  
React Developer Tools](https://chrome.google.com/webstore/detail/react-
developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en)  
  
To view performance metrics for your react app:

Append`?react_perf`to your local server URL (e.g.`localhost:3000/?react_perf`)
and visit that URL in a browser

  
  
  

  

