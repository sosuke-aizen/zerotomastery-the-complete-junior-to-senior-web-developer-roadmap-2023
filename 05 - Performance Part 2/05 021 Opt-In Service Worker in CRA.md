

Starting with Create React App 4, `src/service-worker.js` is opt-in only. What
does that mean? It means that you may not automatically have the service
worker file when you initially create your react app. If you do not see that
file, we will be using it later in the coming videos. To learn how to generate
it, you will have to follow this guide. You can also use this guide as you go
through the rest of this section on PWA to get the latest:  
  
 **  
**[ **https://create-react-app.dev/docs/making-a-progressive-web-
app/**](https://create-react-app.dev/docs/making-a-progressive-web-app)
**(follow this guide!)  
  
You will need to run the command found in the above link to make sure you
generate the appropriate files to create a PWA! Also note that you will only
be able to test this PWA feature once you DEPLOY the app to production (I will
show you how to do this at the end using Github Pages Deploy)  
**

