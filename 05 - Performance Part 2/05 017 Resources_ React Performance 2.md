

To get familiar with the asynchronous nature of setState(), have a look at:  
  
<https://medium.com/@wereHamster/beware-react-setstate-is-asynchronous-
ce87ef1a9cf3>  
  
<https://vasanthk.gitbooks.io/react-bits/patterns/19.async-nature-of-
setState.html>  
  
Finally, have a look at this tool mentioned in the previous video:  
  

Why did you update package now is deprecated and the newer: [Why did you
render?](https://www.npmjs.com/package/@welldone-software/why-did-you-render)
is recommended.

