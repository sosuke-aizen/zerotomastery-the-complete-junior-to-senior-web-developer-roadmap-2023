

Service Worker is implemented very easily into a react based project.You can
see [this Git Diff](https://github.com/jeffposnick/create-react-
pwa/compare/starting-point...pwa) to see what you would need to do to add
service worker into an existing create react app project without the default
service worker.  
  
For further information on the topics covered in the previous video:  
  
[isServiceWorkerReady?](https://jakearchibald.github.io/isserviceworkerready/)  
  
[More information on push notifications for those who want to see how it is
implemented](https://auth0.com/blog/introduction-to-progressive-web-apps-push-
notifications-part-3/)

