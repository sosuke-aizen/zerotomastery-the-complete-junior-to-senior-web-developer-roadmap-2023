

It's time to convert our Robofriends app into a Progressive Web App! Use [this
repo](https://github.com/aneagoie/robofriends-redux) or your version of the
robofriends app with redux to get a perfect 100 score from Lighthouse on all
categories (except performance). Using all of the tools you have learned about
in the previous videos, see if you can work on your own.  
  
If this will be your first time deploying a create react app to github pages,
you can go ahead and watch the next video to learn how to deploy your
robofriends app on github pages so you have a live URL.  
  
Share your lighthouse report in the **  #juniortosenior** Discord channel to
show off once you are done :)  
  
Once you have tried this exercise, you can watch the solution video of what I
did and then also see the finished Github repository with all of the working
code.  
  
Good luck!

  

 **PS the final video in this exercise will show you some of the latest
changes with the Service Worker file that were implemented recently so don't
miss that one where we update our entire project!**

