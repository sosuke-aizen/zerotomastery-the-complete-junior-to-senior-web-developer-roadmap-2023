

In case you want further reading on Progressive Web Apps:  
  
[Submitting PWA to 3 app
stores](http://debuggerdotbreak.judahgabriel.com/2018/04/13/i-built-a-pwa-and-
published-it-in-3-app-stores-heres-what-i-learned/)  
  
[PWA Android vs iOS](https://medium.com/@firt/progressive-web-apps-on-ios-are-
here-d00430dee3a7)  
  
Finally, [here is the link to the robofriends-redux github
repository](https://github.com/aneagoie/robofriends-redux) which we will be
using to turn our app into 100% PWA throughout the next couple of videos.  
  
Ps You can explore some of the top PWAs from around the world:
<https://appsco.pe/>

