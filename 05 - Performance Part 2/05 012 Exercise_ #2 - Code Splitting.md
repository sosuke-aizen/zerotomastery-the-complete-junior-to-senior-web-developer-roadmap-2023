

Now that you have an understanding of Code Splitting, it is time for you to
practice what you have learned in the previous videos and try your hand at
code splitting. Follow this repo: <https://github.com/aneagoie/code-splitting-
exercise> and try to play around and experiment with different Code Splitting
features you have learned.  
  
 **Your Task:**  
Try adding the [`React.lazy`](https://reactjs.org/docs/code-
splitting.html#reactlazy) into the project and share your results in the
**#juniortosenior** channel on Discord! This is now the new recommended way to
do code splitting in a React application as this feature was recently added to
the library:  
<https://www.smooth-code.com/open-source/loadable-components/docs/loadable-vs-
react-lazy/>  
  
  
If you get stuck, you can always refer to the [official code splitting
documentation from React](https://reactjs.org/docs/code-splitting.html)  
  
In the next video, I will show you how we can implement React.lazy!  
  
  

