

Tree shaking is one of those things that will get optimized and improved upon
in the Javascript landscape. For now, though, the best way to use tree shaking
in your apps is to follow this wonderful guide:

<https://developers.google.com/web/fundamentals/performance/optimizing-
javascript/tree-shaking/>

