

Hello everyone!

As of November 7, 2020 the core react team doesn't ship with **PWA** by
default anymore, meaning they won't build your `**service-worker.js**` file in
the final build folder! Do not worry, there are just some additional steps you
will have to run to convert your react app to be able to perform the next step
modifications to be **PWA** compliant!

You only need to do this if you are running react-scripts v.4 or greater or
React v17 or greater and you did not create this project using the **cra-
template-pwa** when we initialized our project.

  

Follow the following steps:

1\. In a new and separate directory from our current project, generate a new
CRA react app with the service worker template:

`npx create-react-app my-app --template cra-template-pwa`

  

2\. Copy the `service-worker.js` and `serviceWorkerRegistration.js` file from
the new created app and add it into our `client/src` directory.

  

3\. Copy the all the "workbox-*" dependencies from the `package.json` file in
the dependencies section and add it into our **client** folders `package.json`
dependencies. Then re-install your packages using yarn or npm depending on
whichever package manager you have been using up to this point!

  

![](https://img-c.udemycdn.com/redactor/raw/article_lecture/2020-12-11_22-04-27-8da4b8a439b78d7dca180f1d4dd69e4e.png)

  

Now you can continue with the remaining videos for progressive web apps!

  

Happy coding everyone :)

