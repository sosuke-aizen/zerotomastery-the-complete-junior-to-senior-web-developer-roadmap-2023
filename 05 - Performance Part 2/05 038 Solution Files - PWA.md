

You can find the solution Github repository at [
**https://github.com/aneagoie/robofriends-
pwa**](https://github.com/aneagoie/robofriends-pwa)  

Or the latest updates included (package.json update) repository at:

[ **https://github.com/aneagoie/Center-For-Robotos-Who-Cant-Be-In-The-App-
Store-And-Wanna-Learn-To-Do-Other-Stuff-Good-Too-
update**](https://github.com/aneagoie/Center-For-Robotos-Who-Cant-Be-In-The-
App-Store-And-Wanna-Learn-To-Do-Other-Stuff-Good-Too-update)  
  
Finally, I would like to share with you this resource to finish off this
section. It is a website that lists all tools that you can use to improve
front end performance of your web app: <https://progressivetooling.com/>

