

You can find all the code used in this section with the latest up to date
libraries below if you encounter any issues:

[ **https://github.com/aneagoie/code-splitting-
exercise**](https://github.com/aneagoie/code-splitting-exercise)

  

[ **https://github.com/aneagoie/code-splitting-exercise-
updated**](https://github.com/aneagoie/code-splitting-exercise-updated)

  

[ **https://github.com/aneagoie/robofriends-
pwa**](https://github.com/aneagoie/robofriends-pwa)

  

[ **https://github.com/aneagoie/Center-For-Robotos-Who-Cant-Be-In-The-App-
Store-And-Wanna-Learn-To-Do-Other-Stuff-Good-Too-
update**](https://github.com/aneagoie/Center-For-Robotos-Who-Cant-Be-In-The-
App-Store-And-Wanna-Learn-To-Do-Other-Stuff-Good-Too-update)

