First, in case you have never put a website online using Github Pages, you can
use this tutorial to have your Github website up and running in 5
minutes:<https://pages.github.com/>Please note that this is only for simple
websites. If you want to have github pages work with create react app, we
cover this in the upcoming video: **Deploying Our React App**  
  
Progressive Web Apps Checklist:
<https://developers.google.com/web/progressive-web-apps/checklist>  

Finally, if you would like to implement HTTPS yourself, I recommend you
use[Let's Encrypt](https://letsencrypt.org/docs/)

