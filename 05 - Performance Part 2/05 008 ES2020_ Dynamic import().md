

Good news! In the next video I am going to show you how to use dynamic
`import()` to help you do code splitting. This feature only worked before
ES2020 by using Webpack and the configuration that allowed Create React App to
use this feature. As of ES2020 this feature is now native to JavaScript so you
can use the trick you are about to learn even outside Create React App!  
  
You can read more about it [here](https://www.ksharifbd.com/blog/exploring-
ES2020-dynamic-import/#Import) _(but I recommend just watching the next
video)_!

