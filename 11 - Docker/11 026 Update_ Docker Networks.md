

Docker is constantly evolving. One of the features that has changed is the
`links` property you have seen in previous videos while writing our docker
compose file. Links have been replaced by networks. Docker describes them as
a[legacy feature that you should avoid
using.](https://docs.docker.com/engine/userguide/networking/default_network/dockerlinks/)You
can safely remove the link and the two containers will be able to refer to
each other by their service name (or container_name). **So in our case all you
would need to do is remove the below lines from your Docker Compose file and
everything should still work fine** :  

    
    
        links:
          - postgres
          - redis

  

By default Compose sets up a
single[network](https://docs.docker.com/engine/reference/commandline/network_create/)for
your app. Each container for a service joins the default network and is both
_reachable_ by other containers on that network, and _discoverable_ by them at
a hostname identical to the container name.If you want a custom network, you
can use the `networks property`.  
If you want to read more about this, you can check out the [official
documentation here](https://docs.docker.com/compose/networking/), or read
[this stackoverflow
answer](https://stackoverflow.com/questions/41294305/docker-compose-
difference-between-network-and-link).  
  
Don't get too confused by this though. This gets into some really advanced
networking that is very specialized. The big takeaway is that you no longer
need the links property.

