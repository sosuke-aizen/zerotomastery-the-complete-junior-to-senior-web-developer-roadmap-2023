

That was a tough video! Lots of material covered, but keep in mind that when
it comes to configuration files like yml files, there is nothing better than
the online documentation to get yourself familiar with them. Although it may
look daunting, most developers just check out and read the documentation when
they have a problem to solve. For example, here are the resources I recommend
you read up on based on the previous video:  
  
1\. How we set up the database with username and password: (see the
environment variables section) <https://hub.docker.com/_/postgres/>  
  
2\. `psql ` command: <https://www.postgresql.org/docs/9.2/static/app-
psql.html>  
  
3\. Finally, if you haven't done so already, see if you can install PostgreSQL
on your machine and use a GUI like I do in the video to make sure your docker-
compose file is working:  
  
  

To install PostgreSQL on your computer, follow the below steps:

 **Mac:  **Follow my previous video for instructions. You will need
[homebrew](https://brew.sh/) for the easiest way to set up. keep in mind you
may have to run with the 'sudo' command.  Or you can follow [this
tutorial](https://www.codementor.io/engineerapart/getting-started-with-
postgresql-on-mac-osx-are8jcopb).

[PSequel GUI](http://www.psequel.com/)

 **Windows:  **Follow [this
tutorial](http://www.postgresqltutorial.com/install-postgresql/)

 **Linux: Thanks to fellow student Dimitris for this great guide:**

For any of the Linux users following the course and interested in installing
PostgreSQL along with a GUI (eg. pgAdmin), [their website has wonderful
instructions](https://www.postgresql.org/download/), and so does their wiki
(for example, [this link is for Debian and Ubuntu based
distros](https://wiki.postgresql.org/wiki/Apt)).  

Also, one way to issue the commands you typed in the video to start, stop,
restart PostgreSQL in Linux is:

    
    
    sudo systemctl start postgresql     # starts the server
    sudo systemctl stop postgresql      # stops it
    sudo systemctl restart postgresql   # restart it
    sudo systemctl status postgresql    # check the server's status
    

The "`createdb test` " command and the "`psql 'test'` " command are the same
(at least for Debian/Ubuntu systems) from what I saw.

When it's first installed, PostgreSQL just has the 'postgres' user, and the
way to initially enter PostgreSQL is by typing  `sudo su - postgres` , and
then `psql` .  After Andrei creates the 'test' database, we can create a user
with the same name as our current logged in user, to be a database
administrator. This way we can just type in `psql 'test'`  from the command
line and enter the database without the need of logging in as the 'postgres'
user, just like Andrei does in the lecture. This can be done with `CREATE USER
your-user-name-here WITH SUPERUSER;` , and we can verify that he was created
with `\du` . Now we can exit by typing `\q`  and then `exit` , and enter our
database just like Andrei does, with `psql 'test'` .

Lastly, with pgAdmin4 we need to create a connection with the server the first
time we use it, and this is done by right-clicking 'Servers' on the left pane,
and choosing 'Create' > 'Server'. We give our server a name, and in the
'Connection' tab we type in 'localhost' as the host, just like Andrei shows in
the lecture, and press 'Save'.

