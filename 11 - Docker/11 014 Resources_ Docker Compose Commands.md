

You can find out more about the 3 commands we just learned here:  
  
[docker-compose build](https://docs.docker.com/compose/reference/build/)  
  
[docker-compose run](https://docs.docker.com/compose/reference/run/)  
  
[docker-compose up](https://docs.docker.com/compose/reference/up/)

