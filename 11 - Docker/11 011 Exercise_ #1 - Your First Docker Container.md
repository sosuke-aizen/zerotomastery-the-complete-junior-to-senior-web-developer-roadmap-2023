

It's time to build your first Docker Container like I did in my previous
video. See if you can set it up on your machine using this [Smart Brain API
repo](https://github.com/aneagoie/smart-brain-api).  
  
Ps, you can learn more about Dockerfile commands by using this
resource:<https://docs.docker.com/engine/reference/builder/#usage>

