

Throughout the next few videos we are going to use Docker Compose to launch
separate services under one command. In these videos, I refer to all of these
being under one container, but technically Docker Compose launches multiple
containers. For simplicity sake, with Docker Compose in the next videos we are
going to assume everything is running in this one "Docker World" as you will
see.

