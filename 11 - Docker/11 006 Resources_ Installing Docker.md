

To install Docker onto your computer:  
  
[For Mac](https://docs.docker.com/docker-for-mac/install/)  
  
[For Windows](https://docs.docker.com/docker-for-windows/install/)  
  
[For Linux](https://docs.docker.com/install/linux/docker-ce/ubuntu/)  
  
If you encounter any errors, you may want to check out [this troubleshooting
guide](https://docs.docker.com/toolbox/faqs/troubleshoot/) or reach out to our
community on Discord!

