

You can find all the code used in this section with the latest up to date
libraries below if you encounter any issues:  
  
  
[ **https://github.com/aneagoie/smart-brain-boost-
api**](https://github.com/aneagoie/smart-brain-boost-api)

  
[ **https://github.com/aneagoie/smart-brain-boost-api-
dockerized**](https://github.com/aneagoie/smart-brain-boost-api-dockerized)
(Heads up that this code already has Redis integrated in it which we will see
in the NEXT section as well as Sessions + JWT)  
  
  
By the way, at the end once we have built our entire app, we are going to have
a lecture titled **Updating Our App** where we will show you how to update
everything to the latest versions of each of these libraries :)

