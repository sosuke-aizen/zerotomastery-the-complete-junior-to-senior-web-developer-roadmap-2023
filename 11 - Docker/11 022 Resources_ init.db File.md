

If you would like to learn more about creating tables in SQL, you can read up
[this excellent post](http://joshualande.com/create-tables-sql).

You can learn more about the` /docker-entrypoint-initdb.d ` which we used in
the previous video in the official [library/postgres image documentation here.  
](https://hub.docker.com/_/postgres/)

