

If you haven't done so already by coding along, go ahead and use Docker
Compose as seen in the previous video to set up both your backend API as well
as the database with some seed data. This way you will be able to use this
docker container for the rest of the sections in the course!

