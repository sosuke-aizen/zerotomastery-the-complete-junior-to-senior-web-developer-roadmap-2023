# ZeroToMastery - The Complete Junior to Senior Web Developer Roadmap (2023)

### 01 - Introduction/:

- 01 001 Course Overview
- 01 004 First Day Of Work

### 02 - SSH 01-02/:

- 02 001 Brunos Request
- 02 002 Introduction to SSH

### 02 - SSH 03-22/:

- 02 003 SSH Command
- 02 006 Saving The Day Through SSH
- 02 009 How SSH Works
- 02 010 Symmetric Encryption
- 02 011 Asymmetric Encryption
- 02 013 Hashing
- 02 014 Passwords Or SSH_
- 02 015 SSH Into A Server
- 02 018 Exercise_ Set Up SSH on Github
- 02 020 Solution_ Set Up SSH on Github
- 02 022 Section Summary

### 03 - Performance Part 1/:

- 03 001 Brunos Request
- 03 003 Introduction to Performance Part 1
- 03 004 3 Keys To Performance
- 03 005 Network Performance
- 03 006 Image File Formats
- 03 008 Image Optimizations
- 03 009 Image Optimizations 2
- 03 011 Delivery Optimizations
- 03 013 Exercise_ #2 - Network Optimizations
- 03 014 Solution_ #2 - Network Optimizations
- 03 015 Critical Render Path Introduction
- 03 016 Critical Render Path 1
- 03 017 Critical Render Path 2
- 03 018 Critical Render Path 3
- 03 020 Critical Render Path 4
- 03 022 Exercise_ #4 - Keiko Corp Website
- 03 024 Solution_ Keiko Corp Website
- 03 027 HTTP_2
- 03 030 Section Summary
- 03 22701806-lecture-files
- 03 22701806-lecture-files.zip
- 03 22701850-lecture-files2
- 03 22701850-lecture-files2.zip
- 03 22702028-lecture-files
- 03 22702028-lecture-files.zip

### 04 - OPTIONAL_ React + Redux + Module Bundling 01-15/:

- 04 001 IMPORTANT_ Thinking Like A Senior Developer
- 04 003 Brunos Request
- 04 004 Section Overview
- 04 006 Angular vs React vs Vue
- 04 009 CWD_ Introduction To React.js
- 04 010 CWD_ Create React App
- 04 011 CWD_ React App Folder Structure
- 04 012 CWD_ React Fundamentals
- 04 013 CWD_ React Fundamentals 2
- 04 014 CWD_ Class vs Functional App.js
- 04 015 CWD_ Hooks vs Classes

### 04 - OPTIONAL_ React + Redux + Module Bundling 17-67/:

- 04 017 CWD_ Your First React Component
- 04 018 CWD_ Building A React App 1
- 04 021 CWD_ Building A React App 2
- 04 022 CWD_ Building A React App 3
- 04 023 CWD_ Styling Your React App
- 04 025 CWD_ Building A React App 4
- 04 026 CWD_ Building A React App 5
- 04 027 CWD_ Building A React App 6
- 04 029 CWD_ Keeping Your Projects Up To Date
- 04 031 Solution_ React 17
- 04 032 CWD_ React Review
- 04 033 CWD_ Error Boundary In React
- 04 034 CWD_ Deploying Our React App
- 04 035 CWD_ React Hooks
- 04 036 CWD_ React Hooks 2
- 04 037 CWD_ React Hooks 3
- 04 038 CWD_ React Hooks 4
- 04 039 CWD_ React Hooks 5
- 04 040 CWD_ React Hooks 6
- 04 041 CWD_ React Hooks 7
- 04 042 CWD_ React Hooks 8
- 04 044 CWD_ React Hooks 9
- 04 045 Introduction To Redux And Webpack
- 04 046 State Management
- 04 047 Why Redux_
- 04 048 Installing Redux
- 04 049 Redux Toolkit
- 04 050 Redux Actions And Reducers
- 04 051 Redux Store And Provider
- 04 052 Redux connect()
- 04 053 Redux Middleware
- 04 054 Redux Async Actions
- 04 055 Redux Project Structures
- 04 057 Popular Tools For React + Redux
- 04 058 Module Bundlers
- 04 059 Introduction To Webpack
- 04 061 Webpack
- 04 062 Updating Libraries_ Babel 7 + ESlint
- 04 065 Parcel
- 04 067 Section Summary
- 04 22757028-robots.js
- 04 22757028-robots.js.zip
04 external-assets-links.txt

### 05 - Performance Part 2 01-11/:

- 05 001 Brunos Request
- 05 002 Section Overview
- 05 004 Optimizing Code
- 05 005 Code Splitting Introduction
- 05 007 Code Splitting Part 1
- 05 009 Code Splitting Part 2
- 05 010 Code Splitting Part 3
- 05 011 Code Splitting Part 4
05 external-assets-links.txt

### 05 - Performance Part 2 13/:

- 05 013 Solution_ React.lazy() Code Splitting

### 05 - Performance Part 2 14-35/:

- 05 014 React Performance Optimizations
- 05 016 React Performance Optimizations 2
- 05 018 Optimizing Code Review
- 05 020 Progressive Web Apps
- 05 023 Progressive Web Apps Examples
- 05 024 PWA - HTTPS
- 05 026 PWA - App Manifest
- 05 028 PWA - Service Workers
- 05 031 PWA - Final Thoughts
- 05 034 Deploying Our React App
- 05 035 Service Worker Updates

### 05 - Performance Part 2 36-39/:

- 05 036 Solution Part 1 - PWA
- 05 037 Solution Part 2 - PWA
- 05 039 Section Summary

### 06 - Testing 01-10/:

- 06 001 Brunos Request
- 06 002 Section Overview
- 06 004 Types of Tests
- 06 005 Testing Libraries
- 06 007 Unit Tests
- 06 008 Integration Tests
- 06 009 Automation Testing
- 06 010 Final Note On Testing

### 06 - Testing 11/:

- 06 011 Setting Up Jest

### 06 - Testing 12-16/:

- 06 012 Our First Tests
- 06 013 Writing Tests
- 06 015 Asynchronous Tests
- 06 016 Asynchronous Tests 2

### 06 - Testing 18/:

- 06 018 Mocks and Spies

### 06 - Testing 22-38/:

- 06 022 Introduction To Enzyme
- 06 025 Snapshot Testing
- 06 026 Snapshot Testing + Code Coverage
- 06 028 Testing Stateful Components
- 06 029 Quick Recap
- 06 031 Testing Connected Components
- 06 032 Testing Connected Components 2
- 06 034 Testing Reducers
- 06 035 Testing Actions
- 06 038 Section Summary

### 07 - TypeScript 01-07/:

- 07 001 Brunos Request
- 07 002 Section Overview
- 07 004 Dynamic vs Static Typing
- 07 005 Weakly vs Strongly Typed
- 07 006 Static Typing In Javascript
- 07 007 TypeScript Compiler

### 07 - TypeScript 10-31/:

- 07 010 TypeScript
- 07 012 TypeScript 2
- 07 013 TypeScript 3
- 07 015 TypeScript 4
- 07 016 TypeScript 5
- 07 017 TypeScript 6
- 07 019 TypeScript 7
- 07 021 TypeScript 8
- 07 022 TypeScript 9
- 07 023 TypeScript 10
- 07 024 DefinitelyTyped
- 07 025 Update_ Create React App with TypeScript
- 07 026 Exercise_ #1 - TypeScript In Robofriends
- 07 028 Solution_ #1 - TypeScript In Robofriends
- 07 030 A Little Joke
- 07 031 Section Summary
- 07 12569250-TypeScript.ts
07 external-assets-links.txt

### 08 - SPA vs Server Side Rendering 01-17/:

- 08 001 Brunos Request
- 08 002 Section Overview
- 08 003 CSR vs SSR
- 08 004 Server Side Rendering React
- 08 006 CSR vs SSR Part 2
- 08 007 SSR React Libraries
- 08 008 Static vs SSR vs CSR_ Gatsby.js vs Next.js vs React.js
- 08 010 Setting Up Next.js
- 08 011 Next.js Pages
- 08 012 Client Side Routing
- 08 014 Shared Components
- 08 015 Dynamic Apps with Next.js
- 08 017 Deploying Next.js Apps
08 external-assets-links.txt

### 08 - SPA vs Server Side Rendering 18-21/:

- 08 018 Updating To Latest Version Of Next.js
- 08 021 Section Summary

### 09 - Security 01-12/:

- 09 001 Brunos Request
- 09 002 Section Overview
- 09 003 Star Of Security
- 09 004 Injections
- 09 008 3rd Party Libraries
- 09 010 Logging
- 09 011 HTTPS Everywhere
- 09 012 XSS + CSRF

### 09 - Security 15/:

- 09 015 Code Secrets

### 09 - Security 16-25/:

- 09 016 Secure Headers
- 09 018 Access Control
- 09 019 Data Management
- 09 021 Don8217t Trust Anyone
- 09 022 Authentication
- 09 025 Section Summary

### 10 - Code Analysis/:

- 10 001 Brunos Request
- 10 002 Section Overview
- 10 005 Setting Up Your Environment
- 10 007 Optional_ CWD - Installing PostgreSQL
- 10 008 How To Analyze Code
- 10 012 Solution_ #3 - Multiple Face Detection
- 10 013 Section Summary

### 11 - Docker/:

- 11 001 Brunos Request
- 11 002 Section Overview
- 11 004 Docker Containers
- 11 005 Installing Docker
- 11 007 Dockerfile
- 11 009 Docker Commands
- 11 010 Dockerfile 2
- 11 013 Docker Compose
- 11 015 Docker-Compose 2
- 11 017 Docker Compose 3
- 11 018 Docker Compose 4
- 11 020 Docker Compose 5
- 11 021 Docker Compose 6
- 11 023 Docker Compose 7
- 11 027 Section Summary

### 12 - Redis/:

- 12 001 Brunos Request
- 12 002 Section Overview
- 12 004 Introduction To Databases
- 12 005 Installing Redis
- 12 007 Redis Commands
- 12 008 Redis Data Types
- 12 009 Redis Hashes
- 12 010 Redis Lists
- 12 011 Redis Sets + Sorted Sets
- 12 012 Section Summary

### 13 - Sessions + JWT/:

- 13 001 Section Overview
- 13 002 Cookies vs Tokens
- 13 004 What We Are Building
- 13 005 JWT
- 13 007 Project Goals
- 13 009 Profile Icon
- 13 011 Profile Dropdown
- 13 013 Profile Styling
- 13 014 Profile Modal 1
- 13 015 Profile Modal 2
- 13 016 Profile Modal 3
- 13 017 Profile Modal 4
- 13 019 Updating Profile 1
- 13 021 Updating Profile 2
- 13 022 Updating Profile 3
- 13 023 User Authentication
- 13 024 Sending The JWT Token
- 13 025 Adding Redis
- 13 027 Solution_ #3 - Adding Redis To Docker Compose
- 13 028 Storing JWT Tokens
- 13 029 Retrieving Auth Token
- 13 030 Client Session Management
- 13 032 Session Sign In
- 13 033 Authorization Middleware
- 13 034 Fixing A Bug
- 13 035 Reviewing Our Code
- 13 036 Updating Our App
- 13 037 Optional_ Why bcrypt-nodejs_
- 13 038 Section Summary

### 14 - AWS/:

- 14 001 Brunos Request
- 14 002 Section Overview
- 14 004 Amazon Web Services
- 14 005 Monolithic vs Micro Services
- 14 006 Amazon Lambda
- 14 007 Amazon Lambda Dashboard
- 14 009 Serverless
- 14 010 Serverless 2
- 14 011 IAM
- 14 013 Deploying A Function
- 14 014 Deploying A Function 2
- 14 015 Deploying A Function 3
- 14 016 Quick Bug Fix
- 14 017 Section Summary
- 14 22702050-handler.js
- 14 22702050-handler.js.zip

### 15 - Performance Part 3/:

- 15 001 Brunos Request
- 15 002 Section Overview
- 15 003 CDNs
- 15 004 GZIP
- 15 005 Database Scaling
- 15 006 Caching 1
- 15 007 Caching 2
- 15 008 Caching 3
- 15 010 Load Balancing
- 15 011 Nginx 1
- 15 013 Nginx 2
- 15 015 Section Summary

### 16 - CI_CD/:

- 16 001 Brunos Request
- 16 002 Section Overview
- 16 004 Continuous Integration, Delivery, Deployment
- 16 005 Building Great Software
- 16 007 CircleCI
- 16 008 Continuous Integration 1
- 16 009 Continuous Integration 2
- 16 011 Building Great Software 2
- 16 012 Section Summary

### 17 - Extra Bits/:

- 17 001 Section Overview
- 17 002 Complexity vs Simplicity
- 17 003 NPM Is Not Your Friend
- 17 004 Learn To Learn
- 17 005 Start With Why

### 18 - The Final Video/:

- 18 003 The One You Have Been Waiting For

### 19 - Extras_ For Windows Users/:

- 19 001 Introduction From Wolfgang
- 19 002 Git for Windows
- 19 003 Install NodeJS for Windows
- 19 004 Install PostgreSQL for Windows
- 19 005 Using PSQL and PGADMIN

### 20 - AMA Video_/:

- 20 001 AMA - 100,000 Students__.encrypted.m4a
- 20 001 AMA - 100,000 Students__.encrypted

### 21 - BONUS SECTION/:

