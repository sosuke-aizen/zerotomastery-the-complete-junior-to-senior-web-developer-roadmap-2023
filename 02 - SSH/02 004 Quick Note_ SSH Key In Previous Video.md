

Heads up! Digital Ocean updated their UI from what you just saw in the video,
so depending on when you are watching this, you may have to do this:  
  
 **Key difference is that** now it's required to add/create a SSH keys or
password before creating a droplet (we will talk about this over the next
couple of videos). Just generate a random one and you should be good to go!

  
What it now looks like:

![](https://img-c.udemycdn.com/redactor/raw/article_lecture/2021-05-24_20-42-49-c94fb2be27c25fb46ca88d3ab4f995e9.png)

  

  

before:

![](https://img-c.udemycdn.com/redactor/raw/article_lecture/2021-05-24_20-42-57-536a1d3f85f82eee4d4d4f3ae723019a.png)

 ** _  
_**

  

