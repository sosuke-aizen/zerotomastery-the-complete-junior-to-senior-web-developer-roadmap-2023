

Heads up: You need your own server in order to use the ssh command. In Lecture
13 I will show you a way to set up a server for free so you can do what I did
in the videos.

Depending on your Operating System, you may need to do a few things different
than me in the video (since I use a Mac). Here are the resources to help you
get set up:  
  
 **Windows:  
** Use PuTTY:<https://mediatemple.net/community/products/dv/204404604/using-
ssh-in-putty->  
Windows 10:<https://www.howtogeek.com/336775/how-to-enable-and-use-
windows-10s-built-in-ssh-commands/>  
Extra:  
1\. <https://www.ssh.com/ssh/putty/windows/>  
2\. <https://www.memset.com/docs/server-security/secure-communication-
ssh/using-ssh-windows/>  
  
**Linux:  
** You should be able to use the `ssh` command already, but in case you can't:  
<https://www.makeuseof.com/tag/beginners-guide-setting-ssh-linux-testing-
setup/>  
  
Also, if you have any issues with this set up, please reach out to our chat
room so that one of us can help you out :)

