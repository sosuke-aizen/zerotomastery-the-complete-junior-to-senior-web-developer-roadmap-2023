

In the previous video I showed you how to add SSH keys to the
`authorized_keys` of a Digital Ocean Server. Afterwards, I also showed you how
to add it on the Digital Ocean control panel.Keep in mind thatyou cannot,use
the control panel in Digital Ocean to add keys to already created droplets.
You either have to create a new server after you've added the SSH key on the
control panel, or add it manually to the `authorized_keys`  
  
Also, if you are on Windows, you may have to use GitBash instead of
Powershell. Here are the steps that a fellow student followed:  
1\. Download, install and open Git Bash - the link is provided at the end of
the video.

2\. Type "ssh-agent bash" and press Enter.

3\. Type"ssh-add ~/.ssh/id_rsa_digitalocean" and press Enter.You should get
confrimation of Identity added.

4\. Try connecting to your digital ocean account again via SSH. Now it
shouldn't ask you for the password!

