

 **Recommended**` **ssh-keygen**` **command:  
**

    
    
    ssh-keygen -t rsa -b 4096 -C "your_email@example.com"

 **  
Windows:  
** If you have Git for Windows (which you should), ssh-keygen command should
be available:<https://gitforwindows.org/>  
You can read more about this
[here](https://stackoverflow.com/questions/11771378/ssh-keygen-is-not-
recognized-as-an-internal-or-external-command)  
Another option is to use <https://www.ssh.com/ssh/putty/windows/puttygen>  
  
`pbcopy` command:<https://superuser.com/questions/472598/pbcopy-for-
windows/1171448#1171448>

  

 **Extra Video:  
** If you want to learn a little bit more about how SSH works internally,
watch this excellent video: <https://youtu.be/ORcvSkgdA58>

