

Ever wonder howhosting websites like Hostgator or Bluehost or GoDaddywork?
Well, using your Digital Ocean Server, you can set one up yourself very
easily. All you need is to ssh into your server and set up your droplet to
serve websites. This is completely optional but if you would like to create
your own websiteserver, then you can follow along this tutorial to see how
everything works underneath the hood. Not bad is it?  
  
<https://www.digitalocean.com/community/tutorials/how-to-set-up-apache-
virtual-hosts-on-ubuntu-16-04>

