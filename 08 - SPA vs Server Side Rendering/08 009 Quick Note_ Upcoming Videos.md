

In the upcoming videos we are going to start using Next.js!  
  
One thing you will notice is that the Next.js version is now a lot higher than
in the videos! I have included an updated lecture at the end of these videos
where I show you some of the latest changes in Next.js and how we can update
our app to the latest version :)

