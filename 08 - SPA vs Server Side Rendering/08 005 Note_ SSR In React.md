

SSR in React can get quite complicated and messy. There are a ton of articles
online that show you how to implement it and you can read those on your
own.However, in this course we are going to be **learning about Next.js**
which we will cover shortly. Inmy opinion it isthe preferredway to create a
server side rendered app becausejust like create-react-app, it allowsus to
focus on the things that matter instead of worrying aboutconfigurations that
always change and aren't important to your development as a programmer. Stay
with me, and I promise it will get more clear how to do SSR properly!

