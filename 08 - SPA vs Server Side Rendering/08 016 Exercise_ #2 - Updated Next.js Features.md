

As you will see in the upcoming video titled **Updating To Latest Version Of
Next.js** , Next.js is constantly getting updated with new features. To
practice how you would read a new feature being added to the language, there
are two tasks for you below:  
  
 **Please note, you need to have version 9.3 of Next.js or higher for these to
work. If you are not using a version that is exactly that or higher than that,
do this exercise AFTER you have completed the lecture: _Updating To Latest
Version Of Next.js  
  
_** 1\. It's recommended to use `getStaticProps` or `getServerSideProps` for
data fetching now instead of `getInitialProps`:  
<https://nextjs.org/docs/api-reference/data-fetching/getInitialProps>  
  
2\. We no longer need to import isomorphic-unfetch. So update the code by
removing it:  
<https://nextjs.org/blog/next-9-4#improved-built-in-fetch-support>  
  
Update your code with the above two features and see that everything still
works!

