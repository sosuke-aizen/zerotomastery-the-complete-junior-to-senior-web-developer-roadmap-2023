

To better understand the difference between Server Side and Client Side
routing, [have a read through this article](https://medium.com/@wilbo/server-
side-vs-client-side-routing-71d710e9227f).  
  
Once you are done, try recreating the effect I demonstrated in the last video:
Create two pages in your next.js project, and try linking between them first
with the <a> tag, then with with <Link> component provided by next. Open up
the Network Tab in your chrome developer tools and see how they are different.  
  
By the way, you may need to do a [hard refresh of the
page](https://www.getfilecloud.com/blog/2015/03/tech-tip-how-to-do-hard-
refresh-in-browsers/) in order to see the proper differences.  
  
As always, if you ever get stuck, you can discuss and reach out in the
**#juniortosenior** Discord channel.

