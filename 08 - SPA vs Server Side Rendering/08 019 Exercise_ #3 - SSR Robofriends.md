

Using what you have learned about Next.js, server side rendering, and React,
**try rebuilding the robofriends app using Next.js**. Then, use the **Network
Tab on Chrome Developer Tools** to analyze the differences between the two.  
  
Finally, use our Discord community and the **#juniortosenior** channel to
discuss your opinions, as well as get help if you get stuck with this
challenge. This is an optional challenge so I will not be providing a solution
video. However, as with most things in a developer's career, we have to learn
to find solutions without having the answer right around the corner. Use any
resources that you want, and share your SSR Robofriends app in the
**#juniortosenior** channel! Looking forward to seeing what you come up with
:)  
  
If you want to start off with the Next.js app I created in the videos, you can
[use this repo](https://github.com/aneagoie/next-ssr) of my code.  
  
 **Bonus:** See if you can add individual robot pages to the robofriends app.

