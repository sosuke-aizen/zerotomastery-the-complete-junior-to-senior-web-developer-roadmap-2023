

Just a heads up that since the next video was created, AWS Lambda uses the
`async` version of the hello function rather than the callback. It instead
looks something like this:  

    
    
    'use strict';
    module.exports.hello = async (event, context) => { 
        return {  
            statusCode: 200,  
            body: JSON.stringify({   
                message: 'Go Serverless v1.0! Your function executed successfully!',   
                input: event,  
            }), 
        }; 
    // Use this code if you don't use the http event with the LAMBDA-PROXY integration 
    // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
    };

  
Underneath it all, it is still doing the same thing as you will see in this
video coming up

