

To learn more about IAM policies, you can check out [the official
documentation from
Amazon](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_manage.html),
or [this
documentation](https://serverless.com/framework/docs/providers/aws/guide/iam/)
from the Serverless framework

