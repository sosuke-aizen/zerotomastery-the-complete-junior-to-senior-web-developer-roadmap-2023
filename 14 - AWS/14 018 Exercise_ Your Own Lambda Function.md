

I hope you are as excited as I am about using AWS Lambda and all the
possibilities you have now. In this exercise, I want to challenge you to be
creative and use a lambda function on your own to add a new feature to the
SmartBrain app, or your own app. Once you are done, please share it in the
Discord **#juniortosenior** channel so the community can see! Just like in the
real world, there won't be a solution file for you at the end of this one. It
either works or it doesn't. Using all you have learned up to this point, using
all of your resources (including our community on Discord).  
  
You can also find the final front end code with the lambda function you just
saw in my videos [here](https://github.com/aneagoie/smart-brain-boost-lambda).
I have also attached the `handler.js`  file from the video to this lecture for
your reference.  
  
As a bonus and if you really want to challenge yourself, try and implement
this feature:

 **Ability for users to upload new pictures to their profile.  **

 **As an idea, you can use an  upload profile button to trigger a lambda
function that uploads the image to an S3 bucket where you store user profile
pictures. Good luck!  
  
  
**

