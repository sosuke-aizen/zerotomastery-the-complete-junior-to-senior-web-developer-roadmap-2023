

Go ahead and install Redis onto your computer so you can follow along in the
upcoming videos. Use [this link](https://redis.io/topics/quickstart) to get
you instructions on installing Redis. If you don't want to install Redis on
your machine, you can also use<https://try.redis.io/> to follow along with the
next videos online.  
  
If you encounter any errors,  
  
<https://stackoverflow.com/questions/37103054/redis-installation-fails-when-
running-make-command>  
  
<https://stackoverflow.com/questions/8131008/issue-with-redis-install>

