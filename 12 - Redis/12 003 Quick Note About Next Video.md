

The next video: _Introduction To Databases_ is  **part of my other course  **[
**"The Complete Web Developer: Zero to
Mastery"**](https://www.udemy.com/course/the-complete-web-developer-zero-to-
mastery/?referralCode=FFF295AECF3594CE440E) **.**  I have decided to include
this one video here in case you do not have any experience with databases to
give you some background on what we are learning in this section.  

