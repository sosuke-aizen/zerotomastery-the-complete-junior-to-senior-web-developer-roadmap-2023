

 ** _IMPORTANT:In this course, we need to be using Node version 8.9.1 or
higher, and NPM_** __ ** _version 5.5.1 or higher!!!!_** If you are having
issues installing NPM and Node.js use the below resources to guide you:

[Installing NPM instructions](https://www.npmjs.com/get-npm) [Node.js install
website](https://nodejs.org/en/download/)

To check if you have Node.js installed, run this command in your terminal:

    
    
    node -v

To confirm that you have npm installed you can run this command in your
terminal:

    
    
    npm -v

If you get the versions number back, everything is working!

While using npm or node commands, if you ever get permission issue, you may
need to run the commands with "sudo" in front of each command. For more
details,[review this](https://docs.npmjs.com/getting-started/fixing-npm-
permissions). And for windows: you may have to[use another command  
](https://stackoverflow.com/questions/9652720/how-to-run-sudo-command-in-
windows)

 **Optional:** You can use a tool like nvm to be able to install multiple
versions of node and change between them for each project. This is advanced so
delve into it at your own risk:[NVMTutorial  
](https://www.sitepoint.com/quick-tip-multiple-versions-node-nvm/)

 **For Windows users:  
**  
 **Wolfgang** , one of our fellow students ( and master of answering questions
in this community) **created a set of videos for Windows users** that you can
watch in order to make sure everything that I do on my Mac, also works on
Windows. Scroll down to the very last section of the course titled: **For
Windows Users** , and check out the **Introduction** video and the
**Installing Node.js for Windows** video![  
  
](https://www.sitepoint.com/quick-tip-multiple-versions-node-nvm/)

