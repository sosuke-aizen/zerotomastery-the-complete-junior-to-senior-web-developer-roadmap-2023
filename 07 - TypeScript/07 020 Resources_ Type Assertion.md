

Type Assertion is another complex topic. If you want to learn more about it, I
recommend you read this:  
  
<https://basarat.gitbook.io/typescript/type-system/type-assertion>

