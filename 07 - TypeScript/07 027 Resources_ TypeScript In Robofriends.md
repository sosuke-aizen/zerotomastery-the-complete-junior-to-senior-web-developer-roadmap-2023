

It's time to get your hands dirty by adding TypeScript to our robofriends app!
You can find the [starting repository right
here](https://github.com/aneagoie/robofriends-typescript). I will also provide
for you a solution video as well as my solution files at the end of this
section.  
  
  
One you have done all you can, post your results and share your thoughts on
TypeScript in the **#typescript** channel of our Discord Community!

