

To try your own SQL Injection, here is a great exercise you can do to see its
power. You may need to login to perform the exercise, but it should be
completely free for you to do this. The exercise can be found at this link:  
  
[SQLInjection](https://www.hacksplaining.com/exercises/sql-injection)

