

It's time to try your hand at doing some bad things... but luckily in a safe
playground. Try the below two exercises to get a better idea of how XSS and
CSRF are used by malicious hackers:  
  
1\. [XSS](https://www.hacksplaining.com/exercises/xss-stored)

2\. [CSRF](https://www.hacksplaining.com/exercises/csrf)

