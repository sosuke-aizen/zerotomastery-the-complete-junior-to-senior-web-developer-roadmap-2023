

For more information on the previous video, I recommend theseresources:  
  
1\. If you are new to HTTP:<https://code.tutsplus.com/tutorials/http-the-
protocol-every-web-developer-must-know-part-1--net-31177>  
2\. To learn a little bit more about HTTP
Headers:<https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers>  
3\. HTTPHeader
Fields:<https://www.tutorialspoint.com/http/http_header_fields.htm>  
4\. Helmet package documentation:<https://github.com/helmetjs/helmet>

