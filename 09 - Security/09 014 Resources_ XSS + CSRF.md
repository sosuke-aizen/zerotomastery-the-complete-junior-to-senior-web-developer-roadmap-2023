

To learn a little bit more about some topics covered in the previous video,
here are my recommended resources:  
  
[Content Security Policy](https://developer.mozilla.org/en-
US/docs/Web/HTTP/CSP)  
  
[Cookies with HTTPOnly and Secure headers](https://developer.mozilla.org/en-
US/docs/Web/HTTP/Cookies)  
  

