

In the previous lecture, plus the upcoming ones, I will be using the security
exercise files I have created. I recommend you clone these repositories and
follow along the videos and play around yourself as well with the security
techniques you learn.  
  
As always, you can discuss any experiments and questions in our Discord
community and going into the **#juniortosenior** channel!  
  
[Front End Code](https://github.com/aneagoie/security-client-exercise)  
[Back End Code](https://github.com/aneagoie/security-server-exercise)

